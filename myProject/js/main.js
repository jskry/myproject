var count =0;
var cells;

var blocks ={
		i:{
			class:"i",
			pattern:[
				[1,1,1,1]
			]
		},
		o:{
			class:"o",
			pattern:[
				[1,1],
				[1,1]
			]
		},
		t:{
			class:"t",
			pattern:[
				[0,1,0],
				[1,1,1]
			]
		},
		s:{
			class:"s",
			pattern:[
				[0,1,1],
				[1,1,0]
			]
		},
		z:{
			class:"z",
			pattern:[
				[1,1,0],
				[0,1,1]
			]
		},
		j:{
			class:"j",
			pattern:[
				[1,0,0],
				[1,1,1]
			]
		},
		l:{
			class:"l",
			pattern:[
				[0,0,1],
				[1,1,1]
			]
		}
};

loadTable();

setInterval(function(){
	count++;
	document.getElementById("hello_text").textContent ="はじめてのJavaScrit("+count+")";

	if(hasFallingBlock()){
	fallBlocks();
	} else {
		deleteRow();
		outer:
		for(var row=0;row< 2;row++){
			for (var col=0;col<10 ;col++){
				if(cells[row][col].className !==""){
					var result = confirm('リトライ');
					if(result) {
						location.reload();
					} else {
						window.open('about:blank','_self').close();
					}
					break outer;
				}
			}
		}
		generateBlock();
	}

},1000);

function loadTable(){
	var td_array = document.getElementsByTagName("td");
	cells =[];
	var index =0;
	for (var row=0;row<20;row++){
		cells[row]=[];
		for (var col=0;col<10;col++){
			cells[row][col]=td_array[index];
			index++;
		}
	}
}

var isFalling = false;
function hasFallingBlock() {
	return isFalling;
}

function fallBlocks() {
	for(var col=0;col<10;col++){
		if(cells[19][col].blockNum === fallingBlockNum){
			isFalling =false;
			return;
		}
	}

	for(var row= 18;row>=0;row--){
		for(var col=0;col<10;col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row+1][col].className !== "" && cells[row+1][col].blockNum !== fallingBlockNum){
					isFalling =false;
					return;
				}
			}
		}
	}
	 for (var row = 18; row >= 0; row--) {
		for (var col = 0; col < 10; col++) {
			if (cells[row][col].blockNum === fallingBlockNum) {
				cells[row + 1][col].className = cells[row][col].className;
		        cells[row + 1][col].blockNum = cells[row][col].blockNum;
		        cells[row][col].className = "";
		        cells[row][col].blockNum = null;
			}
		}
	}
}


var fallingBlockNum=0;
function generateBlock(){
	var keys = Object.keys(blocks);
	  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
	  var nextBlock = blocks[nextBlockKey];
	  var nextFallingBlockNum = fallingBlockNum + 1;

	var pattern = nextBlock.pattern;
	for (var row =0;row<pattern.length;row++){
		for (var col=0 ;col<pattern[row].length;col++){
			if(pattern[row][col]){
				cells[row][col+3].className = nextBlock.class;
				cells[row][col+3].blockNum = nextFallingBlockNum;
			}
		}
	}
	isFalling = true;
	fallingBlockNum = nextFallingBlockNum;
}

//移動
document.addEventListener("keydown",onKeyDown);
function onKeyDown(event) {
	  if (event.keyCode === 37) {
	    moveLeft();
	  } else if (event.keyCode === 39) {
	    moveRight();
	  } else if (event.keyCode === 40) {
		moveDown();
	  }
	}

function moveRight(){
	var isMoveRight =true;
	for(var row=19;row>=0;row--){
		if(cells[row][9].blockNum === fallingBlockNum){
			isMoveRight =false;
			return;
		}
	}

	for(var row= 19;row>=0;row--){
		for(var col=0;col<10;col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row][col+1].className !== "" && cells[row][col+1].blockNum !== fallingBlockNum){
					isMoveRight=false;
					return;
				}
			}
		}
	}
	if(isMoveRight){
	for (var row = 0; row < 20; row++) {
	    for (var col = 9; col >= 0; col--) {
	      if (cells[row][col].blockNum === fallingBlockNum) {
	        cells[row][col + 1].className = cells[row][col].className;
	        cells[row][col + 1].blockNum = cells[row][col].blockNum;
	        cells[row][col].className = "";
	        cells[row][col].blockNum = null;
	        isFalling = true;
	      }
	    }
	  }
	}
}

function moveLeft(){
	var isMoveLeft =true;
	for(var row=19;row>=0;row--){
		if(cells[row][0].blockNum === fallingBlockNum){
			isMoveLeft =false;
			return;
		}
	}

	for(var row= 19;row>=0;row--){
		for(var col=0;col<10;col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row][col-1].className !== "" && cells[row][col-1].blockNum !== fallingBlockNum){
					isMoveLeft=false;
					return;
				}
			}
		}
	}
	if(isMoveLeft){
	for (var row = 0; row < 20; row++) {
	    for (var col = 0; col < 10; col++) {
	      if (cells[row][col].blockNum === fallingBlockNum) {
	        cells[row][col - 1].className = cells[row][col].className;
	        cells[row][col - 1].blockNum = cells[row][col].blockNum;
	        cells[row][col].className = "";
	        cells[row][col].blockNum = null;
	        isFalling = true;
	      }
	    }
	  }
	}
}

function moveDown() {
	for(var col=0;col<10;col++){
		if(cells[19][col].blockNum === fallingBlockNum){
			isFalling =false;
			return;
		}
	}

	for(var row= 18;row>=0;row--){
		for(var col=0;col<10;col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row+1][col].className !== "" && cells[row+1][col].blockNum !== fallingBlockNum){
					isFalling =false;
					return;
				}
			}
		}
	}
	 for (var row = 18; row >= 0; row--) {
		for (var col = 0; col < 10; col++) {
			if (cells[row][col].blockNum === fallingBlockNum) {
				cells[row + 1][col].className = cells[row][col].className;
		        cells[row + 1][col].blockNum = cells[row][col].blockNum;
		        cells[row][col].className = "";
		        cells[row][col].blockNum = null;
			}
		}
	}
}

//消去
function deleteRow(){
	for(var row = 19;row >= 0; row--){
		var canDelete = true;
		for(var col =0;col<10;col++){
			if(cells[row][col].className ===""){
				canDelete =false;
			}
		}
		if(canDelete){
			for (var col = 0;col < 10; col++){
				cells[row][col].className ="";
			}
			for (var downRow = row-1;downRow >= 0; downRow--){
				for (var col=0;col<10;col++){
					cells[downRow +1][col].className =cells[downRow][col].className;
					cells[downRow +1][col].blockNum =cells[downRow][col].blockNum;
					cells[downRow][col].className="";
					cells[downRow][col].blockNum=null;
				}
			}
		}
	}
}
